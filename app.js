import fetch from 'node-fetch';
import fs from "fs";
import pg from "pg";
import dotenv from 'dotenv';
import Enumerable from 'linq'
import date from  'date-and-time'

// Function to fetch data from the REST API
async function fetchDataFromAPI(queryParams) {
  try {
    dotenv.config();
    var headers = {
        'apikey': process.env.apikey,
        'accept': 'application/json'
    };
    var apiUrl =    'https://api.tequila.kiwi.com/v2/search';
    const url = new URL(apiUrl);
    
    var searchParams = new URLSearchParams();
    Object.keys(queryParams).forEach(key => searchParams.append(key, queryParams[key]));
    url.search = searchParams.toString();

    //console.log(url.toString());
    const response = await fetch(url.toString(),
        {
            method: 'GET',
            headers: headers
        }
        );
    if (response.ok) {
      const data = await response.json();
      return data;
    } else {
      throw new Error('API request failed \n '
        + response.statusText + '\n');
    }
  } catch (error) {
    console.error('Error fetching data from API:', error);
    throw error;
  }
}

// Main function
async function main() {
  try {

    let input_tours = fs.readFileSync('input-tours.json');
    let tours = JSON.parse(input_tours);
    let input_source_markets = fs.readFileSync('input-source-markets.json');
    let source_markets = JSON.parse(input_source_markets);
    
    const output_array = [];

    Enumerable.from(tours).forEach(function(tour){  
      Enumerable.from(source_markets)
        .where(function(source_markets) { return tour.country == source_markets.country; })
        .forEach(async function(source_market){
          var queryParams = {
            fly_from: source_market.airport,
            fly_to: tour.airportCode,
            date_from: date.format(date.addDays(date.parse(tour.tour_departure_date,"DD/MM/YYYY"),-2), "DD/MM/YYYY"),
            date_to: tour.tour_departure_date,
            atime_to: "18:00", //arrive by 18:00
            max_stopovers:1,
            return_from: tour.tour_return_date,
            return_to: date.format(date.addDays(date.parse(tour.tour_return_date,"DD/MM/YYYY"),+2), "DD/MM/YYYY"),
            one_for_city: 1
          };
          console.log(JSON.stringify(queryParams));
          var apiData = await fetchDataFromAPI(queryParams);
          //console.log(JSON.stringify(apiData));
          var record =
            {
                "country": tour.country,
                "tour_label": tour.tour_label,
                "fly_from": source_market.airport,
                "fly_to": tour.airportCode,
                //"date_from": queryParams.date_from,
                //"return_from": queryParams.return_from,
                //"date_to": queryParams.date_to,
                //"return_to": queryParams.return_to,
                "price": (apiData.data[0]??{price:"N/A"}).price,
                "local_arrival": (apiData.data[0]??{local_arrival:"N/A"}).local_arrival,
                "local_departure": (apiData.data[0]??{local_departure:"N/A"}).local_departure,
                "route_count": (apiData.data[0]??{route:[0]}).route.length
            };
            console.log(JSON.stringify(record));
        });    
      });

  } catch (error) {
    console.error('An error occurred:', error);
  }
}

// Run the main function
main();
